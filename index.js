// Repetition Control Structures

// While Loop
/*Syntax:
	while(condition){
		statement/s;
	}
*/

/*let count = 5;
// count = 5
while(count !== 0){
	console.log("While: " + count);
	count--;
}
console.log("Displays numbers 1 to 10: ");
count = 1

while(count < 11) {
	console.log("While: " + count);
	count++;
}*/

// Do while loop
/*Syntax:
	do{
		statement;
	} while (condition);
*/
// Number() = is similar to parseInt when converting String into numbers
let number = Number(prompt("Give me a number"));

do {
	console.log("Do While: " + number);
	number += 1;
	// number = number + 1;
} while (number < 10);

// Create a new variable to be used in displaying even numbers from 2 to 10 using while loop

// SOLUTION#1
number = 1;
while (number <= 10) {
	if (number%2 == 0){ 
	console.log("Even: ",number);
	}	
	number++;
}

// SOLUTION#2
let evenNum = 2;
do {
	console.log("EvenNum: " + evenNum);
	evenNum += 2;
} while (evenNum <= 10);

// For Loop
/*Syntax:
	for(initialization;condition;stepExpression){
		statement;
	}
*/
console.log("For Loop")

for (let count = 0; count <= 20; count++) {
	console.log(count);
}

console.log("Even For Loop")
let even = 2
for (let counter = 1; counter <= 5; counter++){
	console.log("Even in For Loop: " +  even);
	even += 2;
}
/* Expected Output:
	Even in For Loop: 2
	Even in For Loop: 4 
	Even in For Loop: 6
	Even in For Loop: 8
	Even in For Loop: 10
*/

console.log("Other for loop examples: ");
let myString = 'alex';
//  .length property is used to count the characters in a string
console.log("Length of string: " + myString.length);

console.log("Index: " + myString[0]);
console.log("Index: " + myString[3]);

// myString.length = 4;
// x = 0

for (let x = 0; x < myString.length; x++) {
	console.log(myString[x]);
}

// reverse
/*for (let x = 0; x < myString.length; x++) {
	console.log(myString[myString.length-1-x]);
}*/

/*for(let y = myString.length; y > 0; y--) {
	console.log(myString[y - 1]);
}*/

// Print out letter individually but will print 3 instead of the vowels

let myName = "AlEx";

for (let i = 0; i < myName.length; i++) {
	if (
		myName[i].toLowerCase() == "a" ||
		myName[i].toLowerCase() == "e" ||
		myName[i].toLowerCase() == "i" ||
		myName[i].toLowerCase() == "o" ||
		myName[i].toLowerCase() == "u"
		) 
		{
			console.log(3);
		}
		else 
		{
			console.log(myName[i]);
		}
}

// Continue and Break Statements

for (let count = 0; count <= 20; count++) {
	// if remainder  is equal to 0 , tells the code to continue to iterate
	if (count % 2 === 0) {
		continue;
	}
	console.log("Continue and Break: " + count);
	// if the current value of count is greater than 10 , the code to stop the loop
	if (count > 10){
		break;
	}
}

let name = 'alexandro'

for (let i = 0; i < name.length; i++) {
	console.log(name[i]);

	if (name[i].toLowerCase() === "a"){
		console.log("Continue to next iteration: ");
		continue;
	}

	if (name[i] === "d") {
		break;
	}
}
